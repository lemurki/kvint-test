// Global styles
import '@styles/normalize.scss';
import '@styles/globals.scss';

function MyApp({ Component, pageProps }) {
  return (
    <div id="page-root">
      {/* Содержимое страницы */}
      <Component {...pageProps} />
    </div>
  );
}

export default MyApp;
