import FrontPageContent from 'components/FrontPageContent';

export default function FrontPage() {
  return (
    <main>
      <FrontPageContent />
    </main>
  );
}
