// styles
import styles from './styles.module.scss';

/**
 * Компонент `ModalContent` отображает содержимое модального окна.
 */
function ModalContent() {
  return (
    <article className={styles.article}>
      <h3 className={styles.title}>Привет</h3>
      <p className={styles.text}>чем могу помочь?</p>
    </article>
  );
}

export default ModalContent;
