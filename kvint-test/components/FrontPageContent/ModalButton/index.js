import PropTypes from 'prop-types';
// styles
import styles from './styles.module.scss';

/**
 * Компонент `ModalButton` отображает кнопку открытия модального окна чата.
 * @property {func} openModal - Функция открытия модального окна.
 * @property {bool} isLoading - Идет загрузка?
 */
function ModalButton({ openModal, isLoading }) {
  return (
    <button
      type="button"
      aria-label="call an assistant"
      aria-haspopup="true"
      className={styles.button}
      onClick={openModal}
      disabled={isLoading}
    >
      Click me!
    </button>
  );
}

ModalButton.propTypes = {
  openModal: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
};

export default ModalButton;
