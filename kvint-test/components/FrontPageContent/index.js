import { useState } from 'react';
// styles
import styles from './styles.module.scss';
// components
import DiaologModal from '@sharedComp/DiaologModal';
import Loader from '@sharedComp/Loader';
import ModalButton from './ModalButton';
import ModalContent from './ModalContent';

function FrontPageContent() {
  const [isDialogModalOpen, setIsDialogModalOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isLoadingSuccses, setIsLoadingSuccses] = useState(false);

  const openModal = () => {
    setIsDialogModalOpen(true);
    setIsLoading(true);

    setTimeout(() => {
      setIsLoading(false);
      setIsLoadingSuccses(true);
    }, 3000);
  };

  const closeModal = () => {
    setIsDialogModalOpen(false);
    setIsLoadingSuccses(false);
  };

  return (
    <div className={styles.container}>
      {!isDialogModalOpen && (
        <ModalButton isLoading={isLoading} openModal={openModal} />
      )}
      {isLoading && <Loader />}
      {isLoadingSuccses && isDialogModalOpen && (
        <DiaologModal closeModal={closeModal}>
          <ModalContent />
        </DiaologModal>
      )}
    </div>
  );
}

export default FrontPageContent;
