import Lottie from 'lottie-react';
// lottie animations
import loaderAnimation from 'public/static/lotties/loaderAnimation.json';
// styles
import styles from './styles.module.scss';

/**
 * Компонент `Loader` отображает анимацию загрузки.
 */
function Loader() {
  return (
    <div className={styles.container} aria-label="Loading">
      <Lottie animationData={loaderAnimation} />
    </div>
  );
}

export default Loader;
