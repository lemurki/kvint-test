import { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
// hooks
import { useOnOutsideClick } from 'hooks/useOnOutsideClick';
// styles
import styles from './styles.module.scss';

/**
 * Компонент `DiaologModal` отображает модальное окно.
 * @property {func} closeModal - Функция скрытия мобального окна.
 * @property {*} [children] - Содержимое модального окна.
 */
function DiaologModal({ closeModal, children }) {
  const modalRef = useRef(null);

  useEffect(() => {
    const handleKeyPress = (e) => {
      if (e.key === 'Escape') closeModal();
    };
    document.addEventListener('keydown', handleKeyPress);
    return () => {
      document.removeEventListener('keydown', handleKeyPress);
    };
  }, [closeModal]);

  useOnOutsideClick(modalRef, () => closeModal());

  return (
    <div ref={modalRef} className={styles.container} role="dialog">
      {children}
    </div>
  );
}

DiaologModal.propTypes = {
  closeModal: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
};

export default DiaologModal;
